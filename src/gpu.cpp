#include <iostream>
#include <chrono>
#include <vector>

int main()
{
    int vec_size = 3;
    std::vector<double> vec_data{std::vector(vec_size, 2.0)};
    for (auto &it : vec_data)
    {
        std::cout << it << " ";
    }

    double *data = vec_data.data();

    auto startTime = std::chrono::steady_clock::now();

#pragma omp target data map(tofrom : data[ : vec_data.size()])
#pragma omp target teams distribute parallel for
    for (unsigned it = 0; it < vec_data.size(); ++it)
    {
        data[it] = data[it] + data[it];
    }

    auto endTime = std::chrono::steady_clock::now();
    std::cout << "GPU: " << std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count() << " ms ; " << std::endl;
    for (auto &it : vec_data)
    {
        std::cout << it << " ";
    }
    return 0;
}
