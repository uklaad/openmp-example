#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>
#include <chrono>
#include <execution>
#include <random>
std::uniform_real_distribution<double> unif(0,1);
   std::default_random_engine re;
   double rd = unif(re);
int vec_size = 500000000;
class Functions
{
public:
    void VectorSerialFor()
    {
        for (auto it = vec_serial.begin(); it != vec_serial.end(); ++it)
        {
            *it += *it;
        }
    }
    void VectorParallelFor()
    {
#pragma omp parallel for
        for (auto it = vec_parallel.begin(); it != vec_parallel.end(); ++it)
        {
            *it += *it;
        }
    }
    void VectorForEach()
    {
        std::for_each(
            std::execution::par_unseq,
            vec_for_each.begin(),
            vec_for_each.end(),
            [](double &it)
            {
                it += it + it/2 + it/3 + it/4 + it/5;
            });
    }
    void VectorGPU()
    {
        double* GPU = vec_GPU.data();

        // size get after pragma does not work, the vec_GPU is not in the scope
        unsigned size = vec_GPU.size();
#pragma omp target data map(tofrom:GPU[:vec_GPU.size()])
#pragma omp target teams distribute parallel for
        for (unsigned it = 0; it < size; ++it)
        {
            GPU[it] = GPU[it]+GPU[it]+GPU[it]/2;
        }
    }
    bool VectorsEqual()
    {
        if (vec_serial == vec_parallel && vec_serial == vec_for_each && vec_serial == vec_GPU)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

private:
    std::vector<double> vec_serial{std::vector(vec_size, rd)};
    std::vector<double> vec_parallel{std::vector(vec_size, rd)};
    std::vector<double> vec_for_each{std::vector(vec_size, rd)};
    std::vector<double> vec_GPU{std::vector(vec_size, rd)};
    
};

class App
{
public:
    void run(int n_of_runs)
    {
        for (int i = 0; i < n_of_runs; i++)
        {
            calc();
        }
        std::cout << std::endl
                  << "Average times from " << n_of_runs << " of runs:" << std::endl;
        std::cout << "Serial: " << std::reduce(vec_serial_times.begin(), vec_serial_times.end()) / vec_serial_times.size() << " ms" << std::endl;
        std::cout << "Parallel_OpenMP: " << std::reduce(vec_parallel_times.begin(), vec_parallel_times.end()) / vec_parallel_times.size() << " ms" << std::endl;
        std::cout << "For_each: " << std::reduce(vec_for_each_times.begin(), vec_for_each_times.end()) / vec_for_each_times.size() << " ms" << std::endl;
        std::cout << "GPU: " << std::reduce(vec_GPU_times.begin(), vec_GPU_times.end()) / vec_GPU_times.size() << " ms" << std::endl;
    }

private:
    void calc()
    {

        startTime = std::chrono::steady_clock::now();
        func.VectorSerialFor();
        endTime = std::chrono::steady_clock::now();
        std::cout << "Serial: " << std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count() << " ms ; ";
        vec_serial_times.push_back(std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count());

        startTime = std::chrono::steady_clock::now();
        func.VectorParallelFor();
        endTime = std::chrono::steady_clock::now();
        std::cout << "Parallel_OpenMP: " << std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count() << " ms ; ";
        vec_parallel_times.push_back(std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count());

        startTime = std::chrono::steady_clock::now();
        func.VectorForEach();
        endTime = std::chrono::steady_clock::now();
        std::cout << "For_each: " << std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count() << " ms ; ";
        vec_for_each_times.push_back(std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count());

        startTime = std::chrono::steady_clock::now();
        func.VectorGPU();
        endTime = std::chrono::steady_clock::now();
        std::cout << "GPU: " << std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count() << " ms" << std::endl;
        vec_GPU_times.push_back(std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime).count());

        if (!func.VectorsEqual())
        {
            std::cout << "They are NOT the same!" << std::endl;
        }
    }
    Functions func;
    std::chrono::steady_clock::time_point startTime;
    std::chrono::steady_clock::time_point endTime;
    std::vector<double> vec_serial_times;
    std::vector<double> vec_parallel_times;
    std::vector<double> vec_for_each_times;
    std::vector<double> vec_GPU_times;
};
int main()
{

    App app;
    app.run(3);

    return 0;
}